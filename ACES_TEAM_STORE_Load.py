#!/usr/bin/env python
# title           :EventList  international.py
# description     :This will Lthrough the jobs  in a loop .
# author          :ajk
# date            :20180814
# version         :1.0
# usage           :python Event_Load_Util.py
# notes           :
# ==============================================================================

import cx_Oracle
from optparse import OptionParser
from CommonLogger import getLogger
from sqlalchemy import create_engine

import ACES_OraConnection
import pandas as pd
from datetime import datetime
import numpy as np
import requests
import os
import ACES_CommonService
import shutil
from os import listdir
from os.path import isfile, join




log = getLogger()
from_addr='aes-services@rgis.com'
# defined command line options
# this also generates --help and error handling
parser = OptionParser()

parser.add_option(
    "-r", "--runType",  # name on the CLI - drop the `--` for positional/required parameters
    type=str,
    dest="runType",
)

# intializations
orgIdList = []
executionOrgId = 0
executionenv = 0
runType = ''


def getOrgId(orgNumber):
    # open a database connection
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.

    statement = "select organization_id from Rgis_Organizations ro  WHERE organization_number   = :orgNumber "
    cursor.execute(statement, {'orgNumber': orgNumber})
    orgId = cursor.fetchone()[0]
    # close the cursor object
    cursor.close()
    # close the connection
    connection.close()
    return orgId


# function to get the events for processing
def loadExcel():
    engine = create_engine('oracle://aes:coldmonth2@sched.rgis.com:1521/sched', echo=False)
   # onlyfiles = [f for f in listdir('D:\apps\ACEMPS\LOADFILE\\') if isfile(join('D:\apps\ACEMPS\LOADFILE\\', f))]
    #for file in onlyfiles:
    fileName = 'CircleKStoreLoad.xlsx'
    sheetName = 'Sheet1'
    log.info("Processing Excel  load  Process - " + fileName)
    df1 = pd.read_excel('D:\\apps\\ACEMPS\\LOADFILE\\' + fileName, sheet_name=sheetName, index=False)


    cols_to_order = ['ZONE_NUMBER', 'TEAM_CODE', 'NEW_STORE', 'CUSTOMER_NUMBER', 'TEAM_NAME']
    df = df1[cols_to_order]

    df = df.drop(
        ["TEAM_CODE",], axis=1)

    df.rename(columns={'ZONE_NUMBER': 'ORGANIZATION_NUMBER',
                       'TEAM_NAME': 'TEAM_CODE',
                        'CUSTOMER_NUMBER': 'CUST_NUMBER',
                       'NEW_STORE':'STORE_NUMBER'
                       }, inplace=True)

    df['ORGANIZATION_NUMBER'] = df['ORGANIZATION_NUMBER'].apply(str)
    df['TEAM_CODE'] = df['TEAM_CODE'].apply(str)
    df['STORE_NUMBER'] = df['STORE_NUMBER'].apply(str)
    df['CUST_NUMBER'] = df['CUST_NUMBER'].apply(str)

    df.ORGANIZATION_NUMBER = pd.to_numeric(df.ORGANIZATION_NUMBER, errors='coerce').fillna(0).astype(np.int64)
    df.columns = df.columns.str.upper()
#
    df.to_sql('ACES_STORE_TEAM_DETAIL', engine, index=False, if_exists='append')
    log.info(" Excel  load completed row count - ")#+ str(df.shape[0]))



if __name__ == '__main__':
    log.info("Starting the EXCEL  load  Process")
    options, arguments = parser.parse_args()
    # optional Arguments
    loadExcel()
    log.info("completed  the EXCEL  load  Process")



