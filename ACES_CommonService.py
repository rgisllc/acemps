#!/usr/bin/env python
# title           :ACES_CommonService.py
# description     :Common Class for the generic functions.
# author          :auzhima
# date            :20171120
# version         :1.0
# usage           :python ACES_CommonService.py
# notes           :
# python_version  :3.6.2
# ==============================================================================


import requests
import json
import cx_Oracle
import logging
import os
import pandas as pd
import time
import ACES_OraConnection

separator = "="
keys = {}

aesWebserviceUrl = ""


def readPropertiesFile(fileName, property):
    # print(fileName)
    curr_dir = os.path.abspath(os.path.dirname(__file__))
    properties_file_Path = curr_dir + '\\' + fileName
    # print('propertiesFilePath' + properties_file_Path)
    with open(properties_file_Path) as f:
        for line in f:
            if separator in line:
                # Find the name and value by splitting the string
                name, value = line.split(separator, 1)

                # Assign key value pair to dict
                # strip() removes white space from the ends of strings
                keys[name.strip()] = value.strip()
    return keys[property]



def aes_time_tracker_start(proccessName):
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    aes_control_id = cursor.var(cx_Oracle.NUMBER)
    cursor.callproc("aes_data_prep_pkg.register_aes_data_prep_start", (proccessName, aes_control_id))
    # print(aes_control_id.getvalue())
    cursor.close()
    # connection.close()
    return aes_control_id.getvalue()


def aes_time_tracker_end(aes_control_id):
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    cursor.callproc("aes_data_prep_pkg.register_aes_data_prep_end", [aes_control_id])
    cursor.close()
    connection.close()


def aes_time_tracker_error(aes_control_id, error_code):
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    cursor.callproc("aes_data_prep_pkg.register_aes_data_prep_error", [aes_control_id, error_code])
    cursor.close()
    connection.close()


def aes_time_tracker_error(aes_control_id, error_code):
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    cursor.callproc("aes_data_prep_pkg.register_aes_data_prep_error", [aes_control_id, error_code])
    cursor.close()
    connection.close()


def getSchedulerJobId(jobName):
    # open a database connection
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.
    cursor.execute("select job_id from AES.OBSDN_JOB where NICKNAME = :name", {'name': jobName})
    jobId = cursor.fetchone()[0]
    # close the cursor object
    cursor.close()
    # close the connection
    connection.close()
    return jobId


def isCursorNone(cur):
    # cur format sample [(None,)]
    # print (cur[0][0])
    if cur[0][0] is None:
        return True
    else:
        return False


def isBlank(myString):
    if myString and myString.strip():
        # myString is not None AND myString is not empty or blank
        return False
    # myString is None OR myString is empty or blank
    return True


def isNotBlank(myString):
    if myString and myString.strip():
        # myString is not None AND myString is not empty or blank
        return True
    # myString is None OR myString is empty or blank
    return False


def checkJobRuntimeStatus(jobRuntimeId):
    # scheduler joburl in properties file http://<Server>:8080/obsidian/rest/job_runtimes/jobRuntimeId
    schedulerJobBaseURL = 'http://schedulerdev:8080/obsidian/rest/jobs/'
    jobRunTimeWebserviceURL = schedulerJobBaseURL + '/job_runtimes/' + str(jobRuntimeId)
    payload = {}
    # Adding header as parameters are being sent in payload
    headers = {'Content-type': 'application/json', 'authorization': 'Basic YWRtaW46cmdpczE5NTgk'}
    requests.packages.urllib3.disable_warnings()
    jobRunTimeResp = requests.api.request('get', jobRunTimeWebserviceURL, data=json.dumps(payload), headers=headers,
                                          verify=False)
    data = jobRunTimeResp.json()
    return data

def getOrgNumber(orgId):
    # open a database connection
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.

    statement = "select organization_number from Rgis_Organizations ro  WHERE  organization_id  = :orgId "
    cursor.execute(statement, {'orgId': orgId})
    org_number = cursor.fetchone()[0]
    # close the cursor object
    cursor.close()
    # close the connection
    connection.close()
    return org_number

def updateDistParams(status='NEW', organizationId=0, statusdesc=''):
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.

    if organizationId != 0 :
        statement = 'UPDATE AES.aces_execution_control  SET STATUS = :status, STATUS_DESCRIPTION=:statusdesc WHERE  ORGANIZATION_ID  = :orgId '
        cursor.execute(statement, {'status': status, 'orgId': organizationId, 'statusdesc': statusdesc })
    # cursor.execute(statement, ('NEW',organizationId))
    print('updating the Status in district Params to status : '+ status)
    #log.info('updating the Status in district Params to status : '+ status)
    connection.commit()
    # close the cursor object
    cursor.close()
    # close the connection
    connection.close()

def get_support_email_adress():
    # open a database connection
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.
    statement = "SELECT default_value   FROM   aes.aes_preferences  WHERE  preference_name = 'ACES_SUPPORT_EMAIL_GRP' "
    cursor.execute(statement)
    email_id = cursor.fetchone()[0]
    # close the cursor object
    cursor.close()
    # close the connection
    connection.close()

    return email_id


def getACESSASOrgInfo(orgId: object) -> object:
    # open a database connection
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.
    if orgId != 0 :
        statement = "select ACES_EXECUTION_STATUS, ACES_SCHEDULE_START_DATE, ACES_SCHEDULE_END_DATE from aes.aces_execution_control  WHERE   ACES_EXECUTION_ACTIVE_FLAG = 'Y' AND ORGANIZATION_ID =:orgId "
        cursor.execute(statement, {'orgId': orgId})
        for _result in cursor:
            _status = _result[0]
            _sched_start_date = _result[1]
            _sched_end_date = _result[2]

        # close the cursor object
    cursor.close()
    # close the connection
    connection.close()
    return _status, _sched_start_date, _sched_end_date


def get_auditor_schedule_details(person_id, sched_start_date, sched_end_date):
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    try:
        cursor = connection.cursor()
        refCursorVar = cursor.var(cx_Oracle.CURSOR)

        try:
            cursor.callproc("aes_apps.aces_application_pkg.GET_EMPLOYEE_EVENTS_DATERANGE",
                            [refCursorVar,person_id,sched_start_date, sched_end_date])
            refCursor = refCursorVar.getvalue()
            #print("Rows:")
            names = [x[0] for x in refCursor.description]
            rows = refCursor.fetchall()
            df= pd.DataFrame(rows, columns=names)

            # df1 = pd.DataFrame(rows)
            # df1.columns = names
            # print("I got %d lines " % len(df1))
        finally:
            if cursor is not None:
                cursor.close()

        #df =  pd.read_sql(sql=query, params={ 'org_id': org_id, 'sched_start_date':sched_start_date, 'sched_end_date':sched_end_date}, con=connection)

    finally:
        if connection is not None:
            connection.close()
    return df


def get_acesorg_list():
    orgList = []
    # open a database connection
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.
    #statement = "select distinct organization_id from AES.AEEL_DISTRICT_PARAMETERS  WHERE  ACTIVE_FLAG = 'Y' AND  EXECUTION_STATUS = 'READY'  order by ORGANIZATION_ID asc"
    statement = "select distinct organization_id from AES.aces_execution_control  WHERE  ACES_EXECUTION_ACTIVE_FLAG = 'Y'  order by ORGANIZATION_ID asc"
    cursor.execute(statement)

    for orgId in cursor.fetchall():
        orgList.append(orgId[0]);

    # close the cursor object
    cursor.close()
    # close the connection
    connection.close()
    return orgList

def aces_execution_prep():
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the procedure.
    cursor.callproc("aes_apps.aces_utility_pkg.aces_execution_control_prep")
    # close the cursor object
    cursor.close()
    return 'SUCCESS'

def getOrgIdToexcute( currStatus='', newStatus=''):
    orgId = 0

    connection = ACES_OraConnection.OraConnection()
    try:
        try_times = 3
        try_number = 1

        while orgId == 0 and try_number <= try_times:

            # prepare a cursor object using cursor() method
            cursor = connection.cursor()
            # create out cursor
            result = cursor.var(cx_Oracle.NUMBER)
            cursor.callfunc('aes_apps.aces_utility_pkg.get_orgid_to_execute', result, [currStatus, newStatus])
            orgId = int(result.getvalue())
            # close the cursor object
            cursor.close()

            if orgId == 0:
                try_number += 1
                time.sleep(3)
    except:
        orgId = 0

    finally:
        if connection is not None:
            connection.close()

    return orgId




def getOrgId(org_number):
    # open a database connection
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.

    statement = "select organization_id  from Rgis_Organizations ro  WHERE  organization_number  = :org_number "
    cursor.execute(statement, {'org_number': org_number})
    orgId = cursor.fetchone()[0]
    # close the cursor object
    cursor.close()
    # close the connection
    connection.close()
    return orgId



def getEmployeeInfo(personId: object) -> object:
    # open a database connection
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.
    if personId != 0 :
        statement = "select full_name, NVL(email_address, BUSINESS_EMAIL_ADDRESS) as email_address ,BADGE_ID from APPS.RGIS_HR_PERSON_V WHERE   person_id = :personId"
        cursor.execute(statement, {'personId': personId})
        for _result in cursor:
            _full_name = _result[0]
            _email_address = _result[1]
            _badge_id = _result[2]

        # close the cursor object
    cursor.close()
    # close the connection
    connection.close()
    return _full_name, _email_address, _badge_id


def get_aces_auditor_list(org_id, sched_start_date, sched_end_date):
    person_id_list = []
    # open a database connection
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.
    #statement = "select distinct person_id from AES.aces_schedule_load_details  WHERE  ORGANIZATION_ID = :org_id and aces_schedule_start_date =:sched_start_date and aces_schedule_end_date =:schedule_end_date  "
    #cursor.execute(statement, { 'org_id': org_id, 'sched_start_date': sched_start_date, 'schedule_end_date': sched_end_date})
    statement = "SELECT DISTINCT  rtd.person_id FROM  apps.rgis_pseudo_teams_detail  rtd, aes.aces_store_team_detail astd WHERE rtd.organization_id =:org_id  AND  rtd.organization_id = astd.organization_id  AND upper(TRIM(astd.team_code)) = upper(TRIM(rtd.team_code)) "
    cursor.execute(statement, { 'org_id': org_id})

    for person_id in cursor.fetchall():
        person_id_list.append(person_id[0]);

    # close the cursor object
    cursor.close()
    # close the connection
    connection.close()
    return person_id_list


def get_auditor_sched_count(person_id, sched_start_date, sched_end_date):
    sched_count = 0
    # open a database connection
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.
    #statement = "select distinct person_id from AES.aces_schedule_load_details  WHERE  ORGANIZATION_ID = :org_id and aces_schedule_start_date =:sched_start_date and aces_schedule_end_date =:schedule_end_date  "
    #cursor.execute(statement, { 'org_id': org_id, 'sched_start_date': sched_start_date, 'schedule_end_date': sched_end_date})
    # create out cursor
    result = cursor.var(cx_Oracle.NUMBER)
    cursor.callfunc('aes_apps.aces_application_pkg.get_schedule_count', result, [person_id, sched_start_date, sched_end_date])
    sched_count = int(result.getvalue())
    # close the cursor object
    cursor.close()
  # close the connection
    connection.close()
    return sched_count


def update_auditor_email_status(person_id, org_id, sched_start_date, sched_end_date):

    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.

    statement = 'UPDATE AES.aces_schedule_load_details  SET  auditor_email_processed_date = trunc(SYSDATE) WHERE  person_id = :person_id  and ORGANIZATION_ID = :org_id '
            #    'and aces_schedule_start_date =:sched_start_date and aces_schedule_end_date =:schedule_end_date   '

    cursor.execute(statement,
                   {'person_id': person_id, 'org_id': org_id})
    #cursor.execute(statement, {'person_id': person_id, 'org_id': org_id, 'sched_start_date': sched_start_date, 'schedule_end_date': sched_end_date})
# cursor.execute(statement, ('NEW',organizationId))
    print('updating the Status in email end date  for persom : '+ person_id)
    #log.info('updating the Status in district Params to status : '+ status)
    connection.commit()
    # close the cursor object
    cursor.close()
    # close the connection
    connection.close()




def updateDistParams(organizationId, status, statusdesc):
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.

    statement = "UPDATE AES.aces_execution_control SET ACES_EXECUTION_STATUS = :status, ACES_EXECUTION_STATUS_DESC = :statusdesc  WHERE  ACES_EXECUTION_ACTIVE_FLAG = 'Y' AND ORGANIZATION_ID =:organizationId  "
    cursor.execute(statement, {'status': status, 'organizationId': organizationId, 'statusdesc':statusdesc})
    print('updating the Status in district Params to status : '+ status)
    connection.commit()
    # close the cursor object
    cursor.close()
    # close the connection
    connection.close()

def checkOrgStatus(orgId, status):
    # open a database connection
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the SQL query using execute() method.
    if orgId != 0 and status != '':
        statement = "select DECODE(ACES_EXECUTION_STATUS,:STATUS, 'Y', 'N')  as STATUSFLAG from AES.aces_execution_control  WHERE  ACES_EXECUTION_ACTIVE_FLAG = 'Y' AND ORGANIZATION_ID =:orgId "
        cursor.execute(statement, {'status': status, 'orgId': orgId})
    statusFlag = cursor.fetchone()[0]
    # close the cursor object
    cursor.close()
    # close the connection
    connection.close()
    return statusFlag

def get_aces_assignment_details(org_id, sched_start_date, sched_end_date):
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    try:
        query = 'select ORGANIZATION_NUMBER,CUSTOMER_NAME,CUSTOMER_NUMBER,YEAR,QUARTER,WEEK, STORE_NUMBER,SCHEDULED_EVENT_ID,SCHEDULED_DATE_TIME,BADGE_ID,EMPLOYEE_NAME,ADDER_CODE  from aes.aces_schedule_load_details  ldet  '
        query += " WHERE   ldet.organization_id = :org_id  and   ldet.aces_schedule_start_date =:sched_start_date and ldet.aces_schedule_end_date = :sched_end_date  " \
                 "order by ldet.SCHEDULED_DATE_TIME, ldet.CUSTOMER_NAME, LDET.STORE_NUMBER, LDET.ADDER_CODE"
        df =  pd.read_sql(sql=query, params={ 'org_id': org_id, 'sched_start_date':sched_start_date, 'sched_end_date':sched_end_date}, con=connection)

    finally:
        if connection is not None:
            connection.close()
    return df