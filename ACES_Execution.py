import pandas as pd
import cx_Oracle
import os
import ACES_OraConnection

from optparse import OptionParser
from CommonLogger import getLogger
import ACES_CommonService

from ACES_CommonEmailService import Email
from jinja2 import Environment, FileSystemLoader
from weasyprint import HTML, CSS
from weasyprint.fonts import FontConfiguration
from datetime import datetime

log = getLogger()
from_addr = 'aes-services@rgis.com'
# defined command line options
# this also generates --help and error handling
parser = OptionParser()

parser.add_option("-r", "--runType", type=str, dest="runType", )

# intializations
orgIdList = []
executionOrgId = 0
executionenv = 0
runType = ''


# function to get the events for processing
def execute_data_prep_sched_load(org_id):
    connection = ACES_OraConnection.OraConnection()
    # prepare a cursor object using cursor() method
    cursor = connection.cursor()
    # execute the procedure.
    cursor.callproc("aes_apps.aces_application_pkg.execute_aces_process", [org_id])
    # close the cursor object
    cursor.close()
    log.info("ACES Employee Schedule Load completed.")
    return 'SUCCESS'


def send_schedule_report_email(org_id):
    aws_access_key = ACES_CommonService.readPropertiesFile('aces.properties', 'aws_access_key')
    aws_secret_key = ACES_CommonService.readPropertiesFile('aces.properties', 'aws_secret_key')
    acesRootFolder = ACES_CommonService.readPropertiesFile('aces.properties', 'aces.load.file.location')
    zoneFolder = ACES_CommonService.readPropertiesFile('aces.properties', 'aces.zone.file.location')
    acesShareFolder = acesRootFolder + zoneFolder

    if not os.path.exists(acesRootFolder ):
        try:
            os.makedirs(acesRootFolder )
        except OSError as e:
            if e.errno != e.EEXIST:
                raise
    if not os.path.exists(acesShareFolder ):
        try:
            os.makedirs(acesShareFolder)
        except OSError as e:
            if e.errno != e.EEXIST:
                raise

    #dist_num = ACES_CommonService.getOrgNumber(org_id)
    _status, _sched_start_date, _sched_end_date = ACES_CommonService.getACESSASOrgInfo(org_id)
    if  _status == 'REPORT_INPROCESS':

        pd.options.mode.chained_assignment = None

        df = ACES_CommonService.get_aces_assignment_details(org_id, _sched_start_date, _sched_end_date)
        if len(df.index) > 0:
            df.head()
            zone_num = df.ORGANIZATION_NUMBER.unique()[0]

            df = df.drop(["ORGANIZATION_NUMBER"], axis=1)

            df["BADGE_ID"] = df["BADGE_ID"].fillna(0).astype(int)
            df['BADGE_ID'] = df['BADGE_ID'].apply(str)
            df['BADGE_ID'].replace('0', ' ', inplace=True)

            df.fillna(value=' ', inplace=True)

           # df.index(["CUSTOMER_NAME","CUSTOMER_NUMBER"], inplace=True)

            #df = df.reset_index().set_index(['CUSTOMER_NAME', 'CUSTOMER_NUMBER', 'STORE_NUMBER','SCHEDULED_EVENT_ID'] , append=True)

            df.rename(columns={'CUSTOMER_NAME': 'CUSTOMER NAME',
                               'CUSTOMER_NUMBER': 'CUSTOMER#',
                               'STORE_NUMBER': 'STORE#',
                               'SCHEDULED_EVENT_ID': 'EVENT ID',
                               'SCHEDULED_DATE_TIME': 'INV DATE TIME',
                               'BADGE_ID': 'BADGE#',
                               'EMPLOYEE_NAME': 'EMPLOYEE NAME',
                               'ADDER_CODE': 'ADDER',
                               }, inplace=True)

            env = Environment(loader=FileSystemLoader('.'))
            template = env.get_template("Emailreport.html")

            date_from_string = f'{_sched_start_date:%Y-%m-%d}'
            date_to_string = f'{_sched_end_date:%Y-%m-%d}'



            subject = 'Schedule  Details for Dist# ' + str(
                zone_num) + ' from ' + date_from_string + ' to ' + date_to_string;
            acesOutFileName = subject +  '.pdf'

            template_vars = {"title": "ACES Report - ", "reportHeaderInfo": subject,
                             "assignment_data": df.to_html(index=False)}

            html_out = template.render(template_vars)

            font_config = FontConfiguration()


            pdf = HTML(string=html_out).write_pdf(stylesheets=[CSS('style.css')], font_config=font_config)

            date_now_string = datetime.now().strftime("%Y-%b-%d")
            if not os.path.exists(acesShareFolder + date_now_string):
                try:
                    os.makedirs(acesShareFolder + date_now_string)
                except OSError as e:
                    if e.errno != e.EEXIST:
                        raise
            if os.path.exists(acesShareFolder + date_now_string):
                f = open(os.path.join(acesShareFolder + date_now_string + '//', acesOutFileName), 'wb')
                f.write(pdf)
                f.close()
            # Email the same

            email_to = 'CoucheTard@RGIS.com'
            email_cc = 'CircleK@RGIS.com'
            email_bcc = 'akuzhima@rgis.com' #SASCommonService.get_support_email_adress()

            if email_to is None:
                email_to = email_bcc

            email = Email(to=email_to, cc=email_cc, bcc=email_bcc, subject=subject)

            BODY_TEXT = "ACES Schedule is Completed for the RGIS District -"
            BODY_TEXT += str(
                zone_num) + "\r\n\n  This is not a monitored email box.\r\n\n\nThank you\r\n AES Team (AES@rgis.com) "

            email.text = BODY_TEXT
            # you could use StringIO.StringIO() to get the file value
            email.add_attachment(pdf)
            email.sendWithAttachment(from_addr=from_addr, file_name=acesOutFileName, ses_access_key=aws_access_key,
                                     ses_secret_key=aws_secret_key)


if __name__ == '__main__':
    log.info("Starting the ACES   Data prep Process")
    options, arguments = parser.parse_args()
    # optional Arguments
    orgId = 0

    if options.runType:
        runType = options.runType

#TODO TEST
    send_schedule_report_email(2247)

    orgId = ACES_CommonService.getOrgIdToexcute('READY', 'DATA_PREP_INPROCESS')

    if orgId != 0 and ACES_CommonService.checkOrgStatus(orgId, 'DATA_PREP_INPROCESS') == 'Y':
        log.info("Starting ACES  executedataprep_sched_load for Org Id : - " + str(orgId) )
        execute_data_prep_sched_load(orgId)
        log.info("SuccessFully Completed execute_data_prep_sched_load for Org Id : - " + str(orgId) )
        ACES_CommonService.updateDistParams(status='REPORT_INPROCESS', organizationId=orgId,
                                          statusdesc='ACES REPORT Generation and Emailing In process')
        send_schedule_report_email(orgId)

        ACES_CommonService.updateDistParams(status='COMPLETE', organizationId=orgId,
                                            statusdesc='ACES LOAD AND REPORT Completed')
        print("SuccessFully Completed send_schedule_report_email for Org Id : - " + str(orgId) )
    else:
        print("Not Running  ACES for Org Id : - " + str(orgId) )



