import pandas as pd
import cx_Oracle
import os
import ACES_OraConnection

from optparse import OptionParser
from CommonLogger import getLogger
import ACES_CommonService

from ACES_CommonEmailService import Email
from jinja2 import Environment, FileSystemLoader
from weasyprint import HTML, CSS
from weasyprint.fonts import FontConfiguration
from datetime import datetime

log = getLogger()
from_addr = 'aes-services@rgis.com'
# defined command line options
# this also generates --help and error handling
parser = OptionParser()

parser.add_option("-r", "--runType", type=str, dest="runType", )

# intializations
orgIdList = []
executionOrgId = 0
executionenv = 0
runType = ''

# function to get the events for processing
def execute_auditor_schedule_email(org_id):
    _status, _sched_start_date, _sched_end_date = ACES_CommonService.getACESSASOrgInfo(org_id)
    person_id_list = ACES_CommonService.get_aces_auditor_list(org_id, _sched_start_date, _sched_end_date)
    for person_id in person_id_list:
        try:
            sched_count = ACES_CommonService.get_auditor_sched_count(person_id, _sched_start_date, _sched_end_date)
            if sched_count > 0  :
                send_auditor_schedule_email(org_id, person_id)
                ACES_CommonService.update_auditor_email_status(person_id, org_id, _sched_start_date, _sched_end_date)
        except (AttributeError, TypeError, ValueError):
            continue

    return 'SUCCESS'


def send_auditor_schedule_email(org_id, person_id):
    aws_access_key = ACES_CommonService.readPropertiesFile('aces.properties', 'aws_access_key')
    aws_secret_key = ACES_CommonService.readPropertiesFile('aces.properties', 'aws_secret_key')
    acesRootFolder = ACES_CommonService.readPropertiesFile('aces.properties', 'aces.load.file.location')
    auditorFolder = ACES_CommonService.readPropertiesFile('aces.properties', 'aces.auditor.file.location')

    acesShareFolder = acesRootFolder+auditorFolder
    if not os.path.exists(acesRootFolder):
        try:
            os.makedirs(acesRootFolder)
        except OSError as e:
            if e.errno != e.EEXIST:
                raise
    if not os.path.exists(acesShareFolder ):
        try:
            os.makedirs(acesShareFolder)
        except OSError as e:
            if e.errno != e.EEXIST:
                raise

    #dist_num = ACES_CommonService.getOrgNumber(org_id)
    _status, _sched_start_date, _sched_end_date = ACES_CommonService.getACESSASOrgInfo(org_id)


    if  _status == 'AUDITOR_SCHEDULE_INPROCESS':

        pd.options.mode.chained_assignment = None

        df = ACES_CommonService.get_auditor_schedule_details(person_id, _sched_start_date, _sched_end_date)
        if len(df.index) > 0:
            df.head()

            _full_name, _email_address, _badge_id = ACES_CommonService.getEmployeeInfo(person_id)

            df = df.drop(
                ["REPSEQID",
                "SCHEDULEDEVENTID",
                "DATEFORMAT",
                "MEETSITEID",
                "STORESTARTTIME",
                "ESTIMATEDLENGTH",
                "EVENTDESCRIPTION",
                "CONFIRM_STATUS",
                "CHANGEREQSTATUS",
                "ISEVENTDRIVER",
                "BANNERNAME",
                "STOREBACKROOMTIME",
                "SCHEDULED_STORE_ID",
                 "MEETSITE",
                 "MEETSITEADDRESS",
                 "MEETTIME",], axis=1)

            df["MEET_SITE"] = "TBD"

            cols_to_order = ['CUSTOMERNAME', 'STORENUMBER', 'STOREADDRESS', 'STOREPHONE', 'SCHEDULEDDATETIME',
                             'MEET_SITE', 'ISEVENTSUPERVISOR']
            new_columns = cols_to_order + (df.columns.drop(cols_to_order).tolist())
            df = df[new_columns]

            df["SCHEDULEDDATETIME"] = pd.to_datetime(df["SCHEDULEDDATETIME"])
            df = df.sort_values(by="SCHEDULEDDATETIME")

            df.rename(columns={'CUSTOMERNAME': 'CUSTOMER NAME',
                               'STORENUMBER': 'STORE#',
                               'STOREADDRESS': 'STORE ADDRESS',
                               'SCHEDULEDDATETIME': 'INVENTORY DATE TIME',
                               'STOREPHONE': 'STORE PHONE#',
                               'ISEVENTSUPERVISOR': 'SUPERVISOR',
                               'MEET_SITE':'MEET SITE',
                               }, inplace=True)

            df.fillna(value=' ', inplace=True)




            env = Environment(loader=FileSystemLoader('.'))
            template = env.get_template("Emailreport.html")

            date_from_string = f'{_sched_start_date:%Y-%m-%d}'
            date_to_string = f'{_sched_end_date:%Y-%m-%d}'

            date_now_string = datetime.now().strftime("%Y-%b-%d")

            subject = 'Schedule Details for Employee  ' + _full_name + ' ( Badge#- ' + str(_badge_id)  + ' ) from ' + date_from_string + ' to ' + date_to_string;
            acesOutFileName = subject +  '.pdf'

            template_vars = {"title": "Schedule Report - ", "reportHeaderInfo": subject,
                             "assignment_data": df.to_html(index=False)}

            html_out = template.render(template_vars)

            font_config = FontConfiguration()


            pdf = HTML(string=html_out).write_pdf(stylesheets=[CSS('style.css')], font_config=font_config)

        if not os.path.exists(acesShareFolder + date_now_string):
            try:
                os.makedirs(acesShareFolder + date_now_string)
            except OSError as e:
                if e.errno != e.EEXIST:
                    raise
        if os.path.exists(acesShareFolder + date_now_string):
            f = open(os.path.join(acesShareFolder + date_now_string + '//', acesOutFileName), 'wb')
            f.write(pdf)
            f.close()
        # Email the same

        email_to = _email_address
        email_cc = 'akuzhima@rgis.com'
        email_bcc =  None#SASCommonService.get_support_email_adress()

        if email_to is None:
            email_to = 'oratest@rgis.com'

        email = Email(to=email_to, cc=email_cc, bcc=email_bcc, subject=subject)

        BODY_TEXT = "Auditor Schedule Report for the Employee  " + _full_name + ' ( Badge#- ' + str(_badge_id)  + ' ) from ' + date_from_string + ' to ' + date_to_string
        BODY_TEXT +=  "\r\n\n  This is not a monitored email box.\r\n\n\nThank you\r\n AES Team (AES@rgis.com) "

        email.text = BODY_TEXT
        # you could use StringIO.StringIO() to get the file value
        email.add_attachment(pdf)
        email.sendWithAttachment(from_addr=from_addr, file_name=acesOutFileName, ses_access_key=aws_access_key,
                                 ses_secret_key=aws_secret_key)


if __name__ == '__main__':
    log.info("Starting the ACES   Auditor SChedule Email Process")
    options, arguments = parser.parse_args()
    # optional Arguments
    orgId = 0

    if options.runType:
        runType = options.runType

#TODO TEST
    #execute_auditor_schedule_email(2287)
    #send_auditor_schedule_email(2287, 634813) #634813

    #COMMEN ONLY FOR INITIAL SEND
    #orgId = ACES_CommonService.getOrgIdToexcute('SCHEDULE_COMPLETE', 'AUDITOR_SCHEDULE_INPROCESS') #AUDITOR_SCHEDULE_INPROCESS
    org_id_list = ACES_CommonService.get_acesorg_list()

    for orgId  in org_id_list:
        ACES_CommonService.updateDistParams(organizationId=orgId, status='AUDITOR_SCHEDULE_INPROCESS',
                                            statusdesc='ACES  AUDITOR_SCHEDULE  Emailing In process')
        if orgId != 0 and ACES_CommonService.checkOrgStatus(orgId, 'AUDITOR_SCHEDULE_INPROCESS') == 'Y':
            log.info("Starting ACES  execute_data_prep_sched_load for Org Id : - " + str(orgId) )
            execute_auditor_schedule_email(orgId)
            log.info("SuccessFully Completed execute_AUDITOR_SCHEDULE_COMPLETE for Org Id : - " + str(orgId) )
            ACES_CommonService.updateDistParams( organizationId=orgId,status='AUDITOR_SCHEDULE_COMPLETE',
                                              statusdesc='ACES  AUDITOR_SCHEDULE  Emailing COMPLETE')



            print("SuccessFully Completed send_schedule_report_email for Org Id : - " + str(orgId) )
        else:
            print("Not Running  ACES for Org Id : - " + str(orgId) )



