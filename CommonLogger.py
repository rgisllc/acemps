#!/usr/bin/env python
# title           :CommonLogger.py
# description     :Common Class for the Logging.
# author          :auzhima
# date            :20171214
# version         :1.0
# usage           :python CommonLogger.py
# notes           :
# python_version  :3.6.2
# ==============================================================================
import os
import logging.config

import logging
import yaml

log_conf_file = 'logging.yaml'
default_level = logging.INFO
default_logger = 'root'
env_key = 'LOG_CFG'


def isNotBlank(myString):
    if myString and myString.strip():
        # myString is not None AND myString is not empty or blank
        return True
    # myString is None OR myString is empty or blank
    return False

def getLogger(logger_name = None):
    # logging.StreamHandler.__init__(self)
    curr_dir = os.path.abspath(os.path.dirname(__file__))
    log_conf_file_Path = curr_dir + '\\' + log_conf_file
    with open(log_conf_file_Path, 'rt') as f:
        config = yaml.safe_load(f.read())
    logging.config.dictConfig(config)
    if isNotBlank(logger_name):
        logger = logging.getLogger(logger_name)

    else:
        logger = logging.getLogger(default_logger)
        logger.addHandler(MyHandler())
    return logger

class MyHandler(logging.StreamHandler):
    def __init__(self):
        logging.StreamHandler.__init__(self)
        curr_dir = os.path.abspath(os.path.dirname(__file__))
        log_conf_file_Path = curr_dir + '\\' + log_conf_file
        with open(log_conf_file_Path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
