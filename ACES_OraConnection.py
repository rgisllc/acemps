#!/usr/bin/env python
# title           :ACES_OraConnection.py
# description     :This will Lthrough the jobs  in a loop .
# author          :ajk
# date            :20171117
# version         :1.0
# usage           :python ACES_OraConnection.py
# notes           :
# python_version  :3.6.2
# ==============================================================================

import cx_Oracle
from ACES_CommonService import readPropertiesFile


class OraConnection(cx_Oracle.Connection):
    def __init__(self):
        #print("Connecting to database")
        aesdburl = readPropertiesFile('aces.properties', 'aesdb.URL')
        aesdbuser = readPropertiesFile('aces.properties', 'aesdb.user')
        aesdbpassword = readPropertiesFile('aces.properties', 'aesdb.password')
        return super(OraConnection, self).__init__(aesdbuser, aesdbpassword, aesdburl)
