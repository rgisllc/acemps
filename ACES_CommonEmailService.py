from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
from dotenv import load_dotenv
from botocore.exceptions import ClientError
import boto3
import boto.ses
import os


CHARSET = "UTF-8"
AWS_REGION = "us-east-1"

class Email(object):
    aws_key = ''
    aws_sceret = ''

    def __init__(self, to,cc,bcc, subject):
        self.to = to
        self.cc= cc
        self.bcc = bcc
        self.subject = subject
        self.text = None
        self.attachment = None


    def text(self, text):
        self.text = text

    def add_attachment(self, attachment):
        self.attachment = attachment

    def sendWithAttachment(self, from_addr=None, file_name = None, ses_access_key= None, ses_secret_key= None):

        aws_access_key = ses_access_key
        aws_secret_key = ses_secret_key

        connection = boto.ses.connect_to_region(
            AWS_REGION,
            aws_access_key_id= aws_access_key,
            aws_secret_access_key=aws_secret_key
        )
        msg = MIMEMultipart()
        msg['Subject'] = self.subject
        msg['From'] = from_addr

        part = MIMEBase('application', "octet-stream")
        part.set_payload(self.attachment)
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment', filename=file_name)
        part.add_header('Content-Type', 'application/vnd.ms-excel; charset=UTF-8')
        msg.attach(part)

        # the message body
        part = MIMEText(self.text)
        msg.attach(part)
        email_destination = []
        email_destination.append(self.to)
        if self.cc is not None:
            email_destination.append(self.cc)

        if self.bcc is not None:
            for id  in self.bcc.split(','):
                email_destination.append(id)
        msg['To'] = ','.join(email_destination)
        return connection.send_raw_email(msg.as_string(), source=from_addr, destinations=email_destination)

    def sendHTMLEmail(self, from_addr=None, body_html=None,ses_access_key= None, ses_secret_key= None):
        # Specify a configuration set. If you do not want to use a configuration
        # set, comment the following variable, and the
        # ConfigurationSetName=CONFIGURATION_SET argument below.
        CONFIGURATION_SET = "ConfigSet"

        # The character encoding for the email.
        CHARSET = "UTF-8"

        aws_access_key =ses_access_key
        aws_secret_key = ses_secret_key

        client = boto3.client('ses', region_name=AWS_REGION,
            aws_access_key_id= aws_access_key,
            aws_secret_access_key=aws_secret_key)
        try:
            # Provide the contents of the email.
            response = client.send_email(
                Destination={
                    'ToAddresses': [self.to],
                    # 'CcAddresses': [self.bcc],
                    'BccAddresses': ['bjoseph@rgis.com','akuzhima@rgis.com']
                },
                Message={
                    'Body': {
                        'Html': {
                            'Charset': CHARSET,
                            'Data': body_html,
                        },
                        # 'Text': {
                        #     'Charset': CHARSET,
                        #     'Data': BODY_TEXT,
                        # },
                    },
                    'Subject': {
                        'Charset': CHARSET,
                        'Data': self.subject,
                    },
                },
                Source=from_addr
            )
        # Display an error if something goes wrong.
        except ClientError as e:
            print(e.response['Error']['Message'])
        else:
            print("Email sent! Message ID:"),
            print(response['MessageId'])

def create_env_vars():
    '''
    Loads environment variables from file AES20_cfg.env for local execution
    '''

    env_params = os.path.abspath(os.path.dirname(__file__))
    env_params = env_params + '//params//sas_solver.env'
    load_dotenv(env_params)



if __name__ == "__main__":

    create_env_vars()
    email = Email(to='akuzhima@rgis.com',cc='',bcc='', subject='SAS REPORT TEST!')

    #BODY_TEXT = "Supervisor Scheduling (SAS) is Completed for the RGIS District - 424.\r\n\n For feedback: email AES@rgis.com. This mailbox is not a support email.\r\n\n\nThank you\r\n SAS Team (AES@rgis.com) "
    sasShareFolder = '//dfs//ns1//PROD//Applications//RSchedule//AES_Reports//SAS//'
    BODY_HTML = """<html>
       <head></head>
       <body>
         <h2>Supervisor Scheduling (SAS) is Completed - """
    BODY_HTML += '  Year ' + str(2020) + ' ' + 'Quarter ' + str(1) + ' Week ' + str(1)
    BODY_HTML += """ </h2>
        <p> <b> Supervisor Scheduling (SAS) Report are stored  and accessible in the network folder : <a href=file://"""
    BODY_HTML += sasShareFolder
    BODY_HTML += """ > Supervisor Scheduling (SAS) Report Location</a> </b></p>
         <p>For feedback: email AES@rgis.com. This is not a monitored email box.</p>
         <p>Thank you,</p>
         <p>AES Team (AES@rgis.com)</p>
       </body>
       </html>
                   """

    email.text = BODY_HTML

    #you could use StringIO.StringIO() to get the file value
    email.add_attachment(open('//dfs//ns1//DEV1//RSchedule/AES_Reports//Zone424W13Q2Y2020.pdf', 'rb').read())
    email.sendHTMLEmail(from_addr='aes-services@rgis.com', body_html=BODY_HTML)
    #email.sendWithAttachment(from_addr='aes-services@rgis.com',file_name="Zone424W13Q2Y2020.pdf")